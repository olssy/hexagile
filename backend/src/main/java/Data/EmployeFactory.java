//Copyright (C) 2017  Nicolas Papanicolaou
//        This program is free software; you can redistribute it and/or modify
//        it under the terms of the GNU General Public License as published by
//        the Free Software Foundation; either version 3 of the License, or
//        (at your option) any later version.

//        This program is distributed in the hope that it will be useful,
//        but WITHOUT ANY WARRANTY; without even the implied warranty of
//        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//        GNU General Public License for more details.
//
//        You should have received a copy of the GNU General Public License
//        along with this program; if not, check https://www.gnu.org/licenses
package Data;

import java.util.Date;

public class EmployeFactory {

    private final String NOM_PATTERN = "[A-Za-z]{1,15}";
    //    private final String REGEX_ADRESSE = "[0-9]{1,4} [A-Za-z]+-{0,1}[A-Za-z]+";
    private final String REGEX_NUMERO_TEL = "\\d{10}";


    public Employe creerEmploye(String prenom, String nom, String adresse, String numeroTelephone, String courriel, String sexe, String postal) throws EmployeInformationException {
//        verificationsInformation(prenom, nom, adresse, numeroTelephone);
        Date date = new Date();
        return new Employe(prenom, nom, adresse, numeroTelephone, courriel, sexe, date, postal);

    }

    private void verificationsInformation(String prenom, String nom, String adresse, String numeroTelephone) throws EmployeInformationException {
        verificationPrenom(prenom);
        verificationNom(nom);
//        verificationAdresse(adresse);
        verificationNumeroTelephone(numeroTelephone);
    }

    void verificationPrenom(String prenom) throws EmployeInformationException {
        if (!prenom.matches(NOM_PATTERN)) {
            throw new EmployeInformationException("Prenom n'est pas valide");
        }
    }

    void verificationNom(String nom) throws EmployeInformationException {
        if (!nom.matches(NOM_PATTERN)) {
            throw new EmployeInformationException("Nom n'est pas valide");
        }
    }

//    void verificationAdresse(String adresse) throws EmployeInformationException {
//        if (!adresse.matches(REGEX_ADRESSE)) {
//            throw new EmployeInformationException("Adresse n'est pas valide");
//        }
//    }

    void verificationNumeroTelephone(String numeroTelephone) throws EmployeInformationException {
        if (!numeroTelephone.matches(REGEX_NUMERO_TEL)) {
            throw new EmployeInformationException("Numero de tel n'est pas valide");
        }

    }
}
