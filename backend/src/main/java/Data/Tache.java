//Copyright (C) 2017  Nicolas Papanicolaou
//        This program is free software; you can redistribute it and/or modify
//        it under the terms of the GNU General Public License as published by
//        the Free Software Foundation; either version 3 of the License, or
//        (at your option) any later version.

//        This program is distributed in the hope that it will be useful,
//        but WITHOUT ANY WARRANTY; without even the implied warranty of
//        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//        GNU General Public License for more details.
//
//        You should have received a copy of the GNU General Public License
//        along with this program; if not, check https://www.gnu.org/licenses

        package Data;

public class Tache {
    private long id;
    private String nom;
    private String description;

    public Tache() {
    }

    public Tache(long id, String nom, String description) {
        this.id = id;
        this.nom = nom;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
