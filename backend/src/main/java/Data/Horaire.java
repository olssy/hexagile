//Copyright (C) 2017  Nicolas Papanicolaou
//        This program is free software; you can redistribute it and/or modify
//        it under the terms of the GNU General Public License as published by
//        the Free Software Foundation; either version 3 of the License, or
//        (at your option) any later version.

//        This program is distributed in the hope that it will be useful,
//        but WITHOUT ANY WARRANTY; without even the implied warranty of
//        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//        GNU General Public License for more details.
//
//        You should have received a copy of the GNU General Public License
//        along with this program; if not, check https://www.gnu.org/licenses
package Data;

import java.util.ArrayList;

public class Horaire {

    private long id;
    private ArrayList<Long> tacheId;
    private long employeId;
    private String jour;
    private String debut;
    private String fin;
    private String commentaire;

    public Horaire() {
    }

    public Horaire(long id, ArrayList<Long> tacheId, long employeId, String jour, String debut, String fin, String commentaire) {
        this.id = id;
        this.tacheId = tacheId;
        this.employeId = employeId;
        this.jour = jour;
        this.debut = debut;
        this.fin = fin;
        this.commentaire = commentaire;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ArrayList<Long> getTacheId() {
        return tacheId;
    }

    public void setTacheId(ArrayList<Long> tacheId) {
        this.tacheId = tacheId;
    }

    public long getEmployeId() {
        return employeId;
    }

    public void setEmployeId(long employeId) {
        this.employeId = employeId;
    }

    public String getJour() {
        return jour;
    }

    public void setJour(String jour) {
        this.jour = jour;
    }

    public String getDebut() {
        return debut;
    }

    public void setDebut(String debut) {
        this.debut = debut;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }


    @Override
    public String toString() {
        return "Horaire{" +
                "id=" + id +
                ", tacheId=" + tacheId +
                ", employeId=" + employeId +
                ", jour=" + jour +
                ", debut=" + debut +
                ", fin=" + fin +
                ", commentaire='" + commentaire + '\'' +
                '}';
    }
    public int getNombreHeures(){
        int nbHeures = 0;
        for (Long tache : tacheId ){
            if (tache > 0){
                nbHeures++;
            }
        }
        return nbHeures;
    }
}
