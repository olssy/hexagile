//Copyright (C) 2017  Nicolas Papanicolaou
//        This program is free software; you can redistribute it and/or modify
//        it under the terms of the GNU General Public License as published by
//        the Free Software Foundation; either version 3 of the License, or
//        (at your option) any later version.

//        This program is distributed in the hope that it will be useful,
//        but WITHOUT ANY WARRANTY; without even the implied warranty of
//        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//        GNU General Public License for more details.
//
//        You should have received a copy of the GNU General Public License
//        along with this program; if not, check https://www.gnu.org/licenses
package Controller.BackEnd;

import Data.Credentials;
import Data.Employe;

import java.sql.*;
import java.util.ArrayList;

class ControllerEmployeBack {


    static int ajouterEmploye(Employe employe) {


        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            String query2 = "INSERT INTO Employe (nom,prenom,adresse,telephone,courriel,login,sexe_id,code_postal) VALUES (?,?,?,?,?,?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(query2, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, employe.getNom());
            preparedStatement.setString(2, employe.getPrenom());
            preparedStatement.setString(3, employe.getAdresse());
            preparedStatement.setString(4, employe.getNumeroTelephone());
            preparedStatement.setString(5, employe.getCourriel());
            preparedStatement.setString(6, employe.getPrenom() + employe.getNom());
            preparedStatement.setString(7, employe.getSexe());
            preparedStatement.setString(8, employe.getPostal());
            preparedStatement.executeUpdate();
            System.out.println(employe.toString());

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    employe.setId(generatedKeys.getLong(1));
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
            conn.close();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    static int supprimerEmploye(long id) {

        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            String query2 = "DELETE FROM Employe WHERE id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(query2);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            System.out.println("DELETE DONE");
            conn.close();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    static int modifierEmploye(Employe employe) {

        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            System.out.println("Employe : ");
            System.out.println(employe.toString());
            String query2 = "UPDATE Employe SET nom = ?,prenom = ?,adresse = ?, telephone = ?,courriel = ?, code_postal = ? WHERE id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(query2);
            preparedStatement.setString(1, employe.getNom());
            preparedStatement.setString(2, employe.getPrenom());
            preparedStatement.setString(3, employe.getAdresse());
            preparedStatement.setString(4, employe.getNumeroTelephone());
            preparedStatement.setString(5, employe.getCourriel());
            preparedStatement.setString(6, employe.getPostal());
            preparedStatement.setLong(7, employe.getId());
            preparedStatement.executeUpdate();
            System.out.println("UPDATE EMPLOYE DONE");
            conn.close();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    static ArrayList<Employe> getEmployes() {


        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            String query = "SELECT * FROM Employe";
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            ArrayList<Employe> employesList = new ArrayList<>();

            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                String prenom = resultSet.getString("prenom");
                String nom = resultSet.getString("nom");
                String adresse = resultSet.getString("adresse");
                String numeroTelephone = resultSet.getString("telephone");
                String courriel = resultSet.getString("courriel");
                String sexe = resultSet.getString("sexe_id");
                String postal = resultSet.getString("code_postal");
                Timestamp timestamp = resultSet.getTimestamp("date_ajout");
                java.util.Date dateEmbauche = new java.util.Date(timestamp.getTime());
                Employe employe = new Employe(prenom, nom, adresse, numeroTelephone, courriel, sexe, dateEmbauche, postal);
                employe.setId(id);
                employesList.add(employe);

            }
            System.out.println("SELECT ALL EMPLOYE DONE");
            conn.close();
            return employesList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    static Employe getEmploye(long id) {


        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            String query = "SELECT * FROM Employe WHERE id = " + id;
            Statement statement = conn.createStatement();

            ResultSet resultSet = statement.executeQuery(query);
            Employe employe = new Employe();
            if (resultSet.next()) {
                employe.setId(resultSet.getLong("id"));
                System.out.println(employe.getId());
                employe.setNom(resultSet.getString("nom"));
                employe.setPrenom(resultSet.getString("prenom"));
                employe.setAdresse(resultSet.getString("adresse"));
                employe.setNumeroTelephone(resultSet.getString("telephone"));
                employe.setSexe(resultSet.getString("sexe_id"));
                employe.setCourriel(resultSet.getString("courriel"));
                employe.setPostal(resultSet.getString("code_postal"));
                Timestamp timestamp = resultSet.getTimestamp("date_ajout");
                java.util.Date date = new java.util.Date(timestamp.getTime());
                employe.setDateEmbauche(date);

                System.out.println("SELECT EMPLOYE DONE");
                conn.close();
                return employe;
            } else {
                return null;
            }


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
