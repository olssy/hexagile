//Copyright (C) 2017  Nicolas Papanicolaou
//        This program is free software; you can redistribute it and/or modify
//        it under the terms of the GNU General Public License as published by
//        the Free Software Foundation; either version 3 of the License, or
//        (at your option) any later version.

//        This program is distributed in the hope that it will be useful,
//        but WITHOUT ANY WARRANTY; without even the implied warranty of
//        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//        GNU General Public License for more details.
//
//        You should have received a copy of the GNU General Public License
//        along with this program; if not, check https://www.gnu.org/licenses
package Controller.BackEnd;

import Data.Employe;
import Data.Horaire;
import Data.Tache;

import java.util.ArrayList;

public class ControllerBackEnd {


    public static int ajouterEmploye(Employe employe) {
        return ControllerEmployeBack.ajouterEmploye(employe);
    }

    public static int supprimerEmploye(long id) {
        return ControllerEmployeBack.supprimerEmploye(id);
    }

    public static int modifierEmploye(Employe employe) {
        return ControllerEmployeBack.modifierEmploye(employe);
    }

    public static ArrayList<Employe> getEmployes() {
        return ControllerEmployeBack.getEmployes();
    }

    public static Employe getEmploye(long id) {
        return ControllerEmployeBack.getEmploye(id);
    }

    public static void ajouterTache(Tache tache) {
        ControllerTacheBack.ajouterTache(tache);
    }

    public static void modifierTache(Tache tache) {
        ControllerTacheBack.modifierTache(tache);
    }

    public static void supprimerTache(long id) {
        ControllerTacheBack.supprimerTache(id);
    }

    public static ArrayList<Tache> getTaches() {
        return ControllerTacheBack.getTaches();
    }

    public static Tache getTache(long id) {
        return ControllerTacheBack.getTache(id);
    }

    public static int ajouterHoraire(Horaire horaire) {
        return ControllerHoraireBack.ajouterHoraire(horaire);
    }

    public static int modifierHoraire(Horaire horaire) {
        return ControllerHoraireBack.modifierHoraire(horaire);
    }

    public static void supprimerHoraire(long id) {
        ControllerHoraireBack.supprimerHoraire(id);
    }

    public static ArrayList<Horaire> getHoraires() {
        return ControllerHoraireBack.getHoraires();
    }

    public static Horaire getHoraire(long id) {
        return ControllerHoraireBack.getHoraire(id);
    }


}