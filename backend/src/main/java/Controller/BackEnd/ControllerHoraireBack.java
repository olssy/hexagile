//Copyright (C) 2017  Nicolas Papanicolaou
//        This program is free software; you can redistribute it and/or modify
//        it under the terms of the GNU General Public License as published by
//        the Free Software Foundation; either version 3 of the License, or
//        (at your option) any later version.

//        This program is distributed in the hope that it will be useful,
//        but WITHOUT ANY WARRANTY; without even the implied warranty of
//        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//        GNU General Public License for more details.
//
//        You should have received a copy of the GNU General Public License
//        along with this program; if not, check https://www.gnu.org/licenses

package Controller.BackEnd;

import Data.Credentials;
import Data.Horaire;
import Data.Tache;

import java.sql.*;
import java.util.ArrayList;

public class ControllerHoraireBack {



    private static String arrayToString(ArrayList<Long> arrayList){
        StringBuilder stringBuilder = new StringBuilder();
        for (Long id : arrayList){
            stringBuilder.append(Long.toString(id));
            stringBuilder.append(" ");
        }

        return stringBuilder.toString();

    }

    static int ajouterHoraire(Horaire horaire) {

        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            String query = "INSERT INTO BlocHoraire (tache_id,employe_id,jour,debut,fin,commentaire) VALUES (?,?,?,?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            String tacheId = arrayToString(horaire.getTacheId());
            preparedStatement.setString(1, tacheId);
            preparedStatement.setLong(2, horaire.getEmployeId());
            preparedStatement.setString(3, "2017-09-20");//FIXME Hard Code for now
            preparedStatement.setString(4, "8:00");//FIXME Hard Code for now
            preparedStatement.setString(5, "9:00");//FIXME Hard Code for now
            preparedStatement.setString(6, horaire.getCommentaire());
            preparedStatement.executeUpdate();

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    horaire.setId(generatedKeys.getLong(1));
                } else {
                    throw new SQLException("Creating horaire failed, no ID obtained.");
                }
            }
            conn.close();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    static int modifierHoraire(Horaire horaire) {

        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            System.out.println("Horaire : ");
            System.out.println(horaire.toString());
            String query2 = "UPDATE BlocHoraire SET tache_id = ?,employe_id = ?, jour = ?, debut = ?, fin = ?, commentaire = ? WHERE employe_id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(query2);
            StringBuilder stringBuilder = new StringBuilder();
            for (Long tache : horaire.getTacheId()){
                stringBuilder.append(tache);
                stringBuilder.append(" ");
            }
            preparedStatement.setString(1, stringBuilder.toString());
            preparedStatement.setLong(2, horaire.getEmployeId());
            preparedStatement.setString(3, "2017-09-20");//FIXME Hard Code for now
            preparedStatement.setString(4, "8:00");//FIXME Hard Code for now
            preparedStatement.setString(5, "9:00");//FIXME Hard Code for now
            preparedStatement.setString(6, horaire.getCommentaire());
            preparedStatement.setLong(7,horaire.getId());
            preparedStatement.executeUpdate();
            System.out.println("UPDATE HORAIRE DONE");
            conn.close();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    static void supprimerHoraire(long id) {

        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            String query2 = "DELETE FROM BlocHoraire WHERE employe_id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(query2);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            System.out.println("DELETE HORAIRE DONE");
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static ArrayList<Horaire> getHoraires() {

        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            String query = "SELECT * FROM BlocHoraire";
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            int horaireCount = 0;
            ArrayList<Horaire> horaireList = new ArrayList<>();
            while (resultSet.next()) {
                horaireCount++;
                long id = resultSet.getLong("id");
                String tacheId = resultSet.getString("tache_id");
                ArrayList<Long> arrayList = stringToArray(tacheId);
                long employeId = resultSet.getLong("employe_id");
                String jour = resultSet.getString("jour");
                String debut = resultSet.getString("debut");
                String fin = resultSet.getString("fin");
                String commentaire = resultSet.getString("commentaire");
                Horaire horaire = new Horaire(id,arrayList,employeId,jour,debut,fin,commentaire);
                horaireList.add(horaire);
            }
            if (horaireCount > 0){
                System.out.println("SELECT ALL HORAIRE DONE");
                conn.close();
                return horaireList;
            }
            else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static ArrayList<Long> stringToArray(String tacheId){
        String[] strings = tacheId.split(" ");
        ArrayList<Long> arrayList = new ArrayList<>();
        for (String string : strings){
            arrayList.add(Long.parseLong(string));
        }
        return arrayList;
    }

    static Horaire getHoraire(long id) {

        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            String query = "SELECT * FROM BlocHoraire WHERE employe_id = " + id;
            Statement statement = conn.createStatement();

            Horaire horaire = new Horaire();
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()) {

                String tacheId = resultSet.getString("tache_id");
                ArrayList<Long> arrayList = stringToArray(tacheId);
                long employeId = resultSet.getLong("employe_id");
                String jour = resultSet.getString("jour");
                String debut = resultSet.getString("debut");
                String fin = resultSet.getString("fin");
                String commentaire = resultSet.getString("commentaire");
                horaire = new Horaire(id,arrayList,employeId,jour,debut,fin,commentaire);
                System.out.println("SELECT HORAIRE DONE");
                conn.close();
                return horaire;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
