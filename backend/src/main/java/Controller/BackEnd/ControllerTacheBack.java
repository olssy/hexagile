//Copyright (C) 2017  Nicolas Papanicolaou
//        This program is free software; you can redistribute it and/or modify
//        it under the terms of the GNU General Public License as published by
//        the Free Software Foundation; either version 3 of the License, or
//        (at your option) any later version.

//        This program is distributed in the hope that it will be useful,
//        but WITHOUT ANY WARRANTY; without even the implied warranty of
//        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//        GNU General Public License for more details.
//
//        You should have received a copy of the GNU General Public License
//        along with this program; if not, check https://www.gnu.org/licenses

package Controller.BackEnd;

import Data.Credentials;
import Data.Tache;

import java.sql.*;
import java.util.ArrayList;

class ControllerTacheBack {


    static void ajouterTache(Tache tache) {

        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            String query2 = "INSERT INTO Tache (nom,description) VALUES (?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(query2, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, tache.getNom());
            preparedStatement.setString(2, tache.getDescription());
            preparedStatement.executeUpdate();
            System.out.println(tache.toString());

            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    tache.setId(generatedKeys.getLong(1));
                } else {
                    throw new SQLException("Creating tache failed, no ID obtained.");
                }
            }
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void modifierTache(Tache tache) {

        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            System.out.println("Employe : ");
            System.out.println(tache.toString());
            String query2 = "UPDATE Tache SET nom = ?,description = ?WHERE id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(query2);
            preparedStatement.setString(1, tache.getNom());
            preparedStatement.setString(2, tache.getDescription());
            preparedStatement.setLong(3, tache.getId());
            preparedStatement.executeUpdate();
            System.out.println("UPDATE TACHE DONE");
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void supprimerTache(long id) {

        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            String query2 = "DELETE FROM Tache WHERE id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(query2);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            System.out.println("DELETE TACHE DONE");
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static ArrayList<Tache> getTaches() {

        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            String query = "SELECT * FROM Tache";
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            ArrayList<Tache> tachelist = new ArrayList<>();
            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                String nom = resultSet.getString("nom");
                String description = resultSet.getString("description");
                Tache tache = new Tache(id, nom, description);
                tachelist.add(tache);

            }
            System.out.println("SELECT ALL TACHES DONE");
            conn.close();
            return tachelist;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    static Tache getTache(long id) {

        try {
            Class.forName(Credentials.driver).newInstance();
            Connection conn = DriverManager.getConnection(Credentials.url + Credentials.dbName, Credentials.userName, Credentials.password);
            String query = "SELECT * FROM Tache WHERE id = " + id;
            Statement statement = conn.createStatement();

            ResultSet resultSet = statement.executeQuery(query);
            Tache tache = new Tache();
            while (resultSet.next()) {
                String nom = resultSet.getString("nom");
                String description = resultSet.getString("description");
                tache.setId(id);
                tache.setDescription(description);
                tache.setNom(nom);

            }
            System.out.println("SELECT TACHE DONE");
            conn.close();
            return tache;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
