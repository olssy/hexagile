//Copyright (C) 2017  Nicolas Papanicolaou
//        This program is free software; you can redistribute it and/or modify
//        it under the terms of the GNU General Public License as published by
//        the Free Software Foundation; either version 3 of the License, or
//        (at your option) any later version.

//        This program is distributed in the hope that it will be useful,
//        but WITHOUT ANY WARRANTY; without even the implied warranty of
//        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//        GNU General Public License for more details.
//
//        You should have received a copy of the GNU General Public License
//        along with this program; if not, check https://www.gnu.org/licenses

package Controller.FrontEnd;

import Controller.BackEnd.ControllerBackEnd;
import Controller.BackEndResponse;
import Data.Employe;
import Data.EmployeFactory;
import Data.EmployeInformationException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

class ControllerEmployeFront {



    static BackEndResponse creationEmploye(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        try {
            HashMap map = FrontEndUtility.getHashMap(request);
            String prenom = map.get("prenom").toString();
            String nom = map.get("nomDeFamille").toString();
            String adresse = map.get("adresse").toString();
            String noTel = map.get("telephone").toString();
            String courriel = map.get("courriel").toString();
            String postal = map.get("postal").toString();
            Employe employe = new EmployeFactory().creerEmploye(prenom, nom, adresse, noTel, courriel, "M",postal);
            int code = ControllerBackEnd.ajouterEmploye(employe);
            backEndResponse.setResponseCode(code);
            backEndResponse.setResponseBody(FrontEndUtility.jsonPropertyBuilder("id", Long.toString(employe.getId())));
            return backEndResponse;
        } catch (EmployeInformationException employeInformationException) {
            System.out.println(employeInformationException.getMessage());
        }

        return backEndResponse;
    }



    static BackEndResponse supprimerEmploye(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        HashMap map = FrontEndUtility.getHashMap(request);
        long id = Long.parseLong(map.get("id").toString());
        int code = ControllerBackEnd.supprimerEmploye(id);
        backEndResponse.setResponseCode(code);
        return backEndResponse;

    }

    static BackEndResponse modifierEmploye(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        HashMap map = FrontEndUtility.getHashMap(request);
        long id = Long.parseLong(map.get("id").toString());
        String prenom = map.get("prenom").toString();
        String nom = map.get("nom").toString();
        String adresse = map.get("adresse").toString();
        String telephone = map.get("telephone").toString();
        String courriel = map.get("courriel").toString();
        Employe employe = ControllerBackEnd.getEmploye(id);
        int code = -1;
        if (employe != null){
            if (!nom.isEmpty()) employe.setNom(nom);
            if (!prenom.isEmpty()) employe.setPrenom(prenom);
            if (!telephone.isEmpty()) employe.setNumeroTelephone(telephone);
            if (!adresse.isEmpty()) employe.setAdresse(adresse);
            if (!courriel.isEmpty()) employe.setCourriel(courriel);
            code = ControllerBackEnd.modifierEmploye(employe);
        }
        backEndResponse.setResponseCode(code);
        return backEndResponse;

    }



    static BackEndResponse getEmploye(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        long id = Long.parseLong(request.getParameter("numeroEmploye"));
        String[] keys = {"nom", "prenom", "adresse", "numeroTelephone", "courriel","postal"};
        Employe employe = ControllerBackEnd.getEmploye(id);
        int code = -1;
        if (employe != null){
            code = 0;
            String[] values = {employe.getNom(), employe.getPrenom(), employe.getAdresse(), employe.getNumeroTelephone(), employe.getCourriel(),employe.getPostal()};
            String employeJson = FrontEndUtility.jsonPropertyBuilder(keys, values);
            backEndResponse.setResponseBody(employeJson);
        }
        backEndResponse.setResponseCode(code);
        return backEndResponse;

    }

    static BackEndResponse getEmployes(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        ArrayList<Employe> listEmploye = ControllerBackEnd.getEmployes();
        int code = -1;
        if (listEmploye != null){
            code = 0;
            ArrayList<String> listJson = new ArrayList<>();
            String[] keys = {"id", "nom", "prenom", "adresse", "telephone", "courriel","postal"};

            for (Employe employe : listEmploye) {
                String[] values = {Long.toString(employe.getId()), employe.getNom(), employe.getPrenom(), employe.getAdresse(), employe.getNumeroTelephone(), employe.getCourriel(),employe.getPostal()};
                String employeJson = FrontEndUtility.jsonPropertyBuilder(keys, values);
                listJson.add(employeJson);
            }
            String response = FrontEndUtility.jsonPropertyBuilder("employes", listJson);
            backEndResponse.setResponseBody("{" + response + "}");
        }
        backEndResponse.setResponseCode(code);
        return backEndResponse;

    }


}
