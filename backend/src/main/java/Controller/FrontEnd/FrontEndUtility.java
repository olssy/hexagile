//Copyright (C) 2017  Nicolas Papanicolaou
//        This program is free software; you can redistribute it and/or modify
//        it under the terms of the GNU General Public License as published by
//        the Free Software Foundation; either version 3 of the License, or
//        (at your option) any later version.

//        This program is distributed in the hope that it will be useful,
//        but WITHOUT ANY WARRANTY; without even the implied warranty of
//        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//        GNU General Public License for more details.
//
//        You should have received a copy of the GNU General Public License
//        along with this program; if not, check https://www.gnu.org/licenses
package Controller.FrontEnd;

import Controller.BackEndResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

class FrontEndUtility {

    static void responseBuilderJSON(HttpServletResponse response, BackEndResponse backEndResponse, Status status) throws IOException {
        if (backEndResponse.getResponseCode() == -1) {
            response.setStatus(status.badStatus);
        } else {
            response.setStatus(status.goodStatus);
            if (backEndResponse.getResponseBody() != null) {
                response.getWriter().write(backEndResponse.getResponseBody());
                response.setContentType("application/json");
            }
        }
    }

    static void responseBuilderARRAY(HttpServletResponse response, BackEndResponse backEndResponse, Status status) throws IOException {
        if (backEndResponse.getResponseCode() == -1) {
            response.setStatus(status.badStatus);
        } else {
            response.setStatus(status.goodStatus);
            if (backEndResponse.getResponseBody() != null) {
                response.getWriter().write(backEndResponse.getResponseBody());
            }
        }
    }

    static String jsonPropertyBuilder(String key, String value) {
        return "{\"" + key + "\": \"" + value + "\" }";
    }

    static String jsonPropertyBuilder(String key, List<String> values) {

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < values.size(); i++) {
            stringBuilder.append(values.get(i));
            if (i + 1 != values.size()) {
                stringBuilder.append(",");
            }
        }

        return "\"" + key + "\": " + "[" + stringBuilder.toString() + "]";
    }

    static String jsonPropertyBuilder(String keys[], String values[]) {
        ArrayList<String> strings = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < keys.length; i++) {
            stringBuilder.append("\"" + keys[i] + "\": \"" + values[i] + "\"");
            if (i + 1 != keys.length) {
                stringBuilder.append(",");
            }
        }

        return "{" + stringBuilder.toString() + "}";

    }

    static String jsonPropertyBuilderArray(String employe, Object values[]) {
        ArrayList<String> strings = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("\"" + employe + "\",");
        for (int i = 0; i < values.length; i++) {
            stringBuilder.append("\"" + values[i].toString() + "\"");
            if (i + 1 != values.length) {
                stringBuilder.append(",");
            }
        }

        return "[" + stringBuilder.toString() + "]";
    }

    static HashMap getHashMap(HttpServletRequest request) {
        Enumeration keys = request.getParameterNames();
        HashMap map = new HashMap();
        String key = null;
        while (keys.hasMoreElements()) {
            key = keys.nextElement().toString();
            String[] strings = request.getParameterValues(key);
            if (strings.length == 1) {
                map.put(key, request.getParameter(key));
            } else {
                map.put(key, request.getParameterValues(key));
            }
        }
        return map;
    }


}
