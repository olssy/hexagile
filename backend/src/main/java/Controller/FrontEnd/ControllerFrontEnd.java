//Copyright (C) 2017  Nicolas Papanicolaou
//        This program is free software; you can redistribute it and/or modify
//        it under the terms of the GNU General Public License as published by
//        the Free Software Foundation; either version 3 of the License, or
//        (at your option) any later version.

//        This program is distributed in the hope that it will be useful,
//        but WITHOUT ANY WARRANTY; without even the implied warranty of
//        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//        GNU General Public License for more details.
//
//        You should have received a copy of the GNU General Public License
//        along with this program; if not, check https://www.gnu.org/licenses

package Controller.FrontEnd;


import Controller.BackEndResponse;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ControllerFrontEnd extends HttpServlet {

    private static final String ajouterEmployePath = "/api/ajouterEmploye";
    private static final String supprimerEmployePath = "/api/supprimerEmploye";
    private static final String modifierEmployePath = "/api/modifierEmploye";
    private static final String getEmployePath = "/api/getEmploye";
    private static final String getListeEmployePath = "/api/getEmployes";
    private static final String ajouterTachePath = "/api/ajouterTache";
    private static final String supprimerTachePath = "/api/supprimerTache";
    private static final String modifierTachePath = "/api/modifierTache";
    private static final String getTachePath = "/api/getTache";
    private static final String getListeTachePath = "/api/getTaches";
    private static final String ajouterHorairePath = "/api/ajouterHoraire";
    private static final String supprimerHorairePath = "/api/supprimerHoraire";
    private static final String modifierHorairePath = "/api/modifierHoraire";
    private static final String getHorairePath = "/api/getHoraire";
    private static final String getListeHorairePath = "/api/getHoraires";



    /*
        /api/ajouterEmployer POST nom, prenom, etc. Succes: 200 avec le id de l'employer dans le BODY Echec: 409 si l'employer existe deja
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String servletPath = request.getServletPath();
        if (servletPath.equalsIgnoreCase(ajouterEmployePath)) {
            ajouteEmploye(request, response);
        } else if (servletPath.equalsIgnoreCase(modifierEmployePath)) {
            modifierEmploye(request, response);
        } else if (servletPath.equalsIgnoreCase(ajouterTachePath)) {
            ajouterTache(request, response);
        } else if (servletPath.equalsIgnoreCase(modifierTachePath)) {
            modifierTache(request, response);
        } else if (servletPath.equalsIgnoreCase(ajouterHorairePath)) {
            ajouterHoraire(request,response);
        } else if (servletPath.equalsIgnoreCase(modifierHorairePath)){
            modifierHoraire(request,response);
        } else if (servletPath.equalsIgnoreCase(supprimerEmployePath)) {
            supprimerEmploye(request, response);
        }
    }


    /*
        /api/supprimerEmployer{id-employer} DELETE Succes: 200 Echec: 404
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String servletPath = request.getServletPath();
        if (servletPath.equalsIgnoreCase(supprimerEmployePath)) {
            supprimerEmploye(request, response);
        } else if (servletPath.equalsIgnoreCase(supprimerTachePath)) {
            supprimerTache(request, response);
        } else if (servletPath.equalsIgnoreCase(supprimerHorairePath)){
            supprimerHoraire(request,response);
        }

    }

    /*
                /api/modifierEmployer/{id-employer} PUT nom, prenom, etc. Succes: 200 Erreur: 404
    */
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String servletPath = request.getServletPath();

    }

    /*
        /api/getEmployer/{id-employer} GET Succes: 200 Erreur: 404
        /api/getEmployers GET Succes: 200 Echec: ?
    */

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String servletPath = request.getServletPath();
        if (servletPath.equalsIgnoreCase(getEmployePath)) {
            getEmploye(request, response);
        } else if (servletPath.equalsIgnoreCase(getListeEmployePath)) {
            getEmployes(request, response);
        } else if (servletPath.equalsIgnoreCase(getTachePath)) {
            getTache(request, response);
        } else if (servletPath.equalsIgnoreCase(getListeTachePath)) {
            getTaches(request, response);
        } else if (servletPath.equalsIgnoreCase(getHorairePath)){
            getHoraire(request,response);
        } else if (servletPath.equalsIgnoreCase(getListeHorairePath)){
            getHoraires(request,response);
        }

    }





    /*
    =============================================================================================================================================
    =============================================================================================================================================
    =============================================================================================================================================
    =============================================================================================================================================
    =============================================================================================================================================
    =============================================================================================================================================
    =============================================================================================================================================

     */

    private void modifierEmploye(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse;
        System.out.println("Modifier Employe");
        backEndResponse = ControllerEmployeFront.modifierEmploye(request);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_NOT_FOUND));
    }

    private void ajouteEmploye(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse;
        backEndResponse = ControllerEmployeFront.creationEmploye(request);
        Status status = new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_CONFLICT);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, status);
    }

    private void supprimerEmploye(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse = ControllerEmployeFront.supprimerEmploye(request);
        Status status = new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_NOT_FOUND);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, status);
    }


    private void getEmployes(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse;
        backEndResponse = ControllerEmployeFront.getEmployes(request);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_NOT_FOUND));
    }

    private void getEmploye(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse;
        backEndResponse = ControllerEmployeFront.getEmploye(request);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_NOT_FOUND));
    }

    private void ajouterTache(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse;
        backEndResponse = ControllerTacheFront.creationTache(request);
        Status status = new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_CONFLICT);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, status);
    }

    private void modifierTache(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse;
        System.out.println("Modifier Tache");
        backEndResponse = ControllerTacheFront.modifierTache(request);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_NOT_FOUND));
    }


    private void supprimerTache(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse = ControllerTacheFront.supprimerTache(request);
        Status status = new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_NOT_FOUND);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, status);
    }


    private void getTaches(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse;
        backEndResponse = ControllerTacheFront.getTaches(request);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_NOT_FOUND));
    }

    private void getTache(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse;
        backEndResponse = ControllerTacheFront.getTache(request);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_NOT_FOUND));
    }


    private void ajouterHoraire(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse;
        backEndResponse = ControllerHoraireFront.creationHoraire(request);
        Status status = new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_CONFLICT);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, status);
    }

    private void modifierHoraire(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse;
        System.out.println("Modifier Horaire");
        backEndResponse = ControllerHoraireFront.modifierHoraire(request);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_NOT_FOUND));
    }


    private void supprimerHoraire(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse = ControllerHoraireFront.supprimerHoraire(request);
        Status status = new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_NOT_FOUND);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, status);
    }


    private void getHoraires(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse;
        backEndResponse = ControllerHoraireFront.getHoraires(request);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_NOT_FOUND));
    }

    private void getHoraire(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BackEndResponse backEndResponse;
        backEndResponse = ControllerHoraireFront.getHoraire(request);
        FrontEndUtility.responseBuilderJSON(response, backEndResponse, new Status(HttpServletResponse.SC_OK, HttpServletResponse.SC_NOT_FOUND));
    }

}
