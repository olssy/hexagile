//Copyright (C) 2017  Nicolas Papanicolaou
//        This program is free software; you can redistribute it and/or modify
//        it under the terms of the GNU General Public License as published by
//        the Free Software Foundation; either version 3 of the License, or
//        (at your option) any later version.

//        This program is distributed in the hope that it will be useful,
//        but WITHOUT ANY WARRANTY; without even the implied warranty of
//        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//        GNU General Public License for more details.
//
//        You should have received a copy of the GNU General Public License
//        along with this program; if not, check https://www.gnu.org/licenses
package Controller.FrontEnd;

import Controller.BackEnd.ControllerBackEnd;
import Controller.BackEndResponse;
import Data.Tache;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

public class ControllerTacheFront {


    static BackEndResponse creationTache(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        try {
            HashMap map = FrontEndUtility.getHashMap(request);
            String nom = map.get("nom").toString();
            String description = map.get("description").toString();
            Tache tache = new Tache(0, nom, description);
            ControllerBackEnd.ajouterTache(tache);
            backEndResponse.setResponseCode(1);
            backEndResponse.setResponseBody(FrontEndUtility.jsonPropertyBuilder("id", Long.toString(tache.getId())));
            return backEndResponse;
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

        return backEndResponse;
    }


    static BackEndResponse supprimerTache(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        long id = Long.parseLong(request.getParameter("id"));
        ControllerBackEnd.supprimerTache(id);
        backEndResponse.setResponseCode(1);
        return backEndResponse;

    }

    static BackEndResponse modifierTache(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        HashMap map = FrontEndUtility.getHashMap(request);
        long id = Long.parseLong(map.get("id").toString());
        String nom = map.get("nom").toString();
        String description = map.get("description").toString();
        Tache tache = ControllerBackEnd.getTache(id);
        if (!nom.isEmpty()) tache.setNom(nom);
        if (!description.isEmpty()) tache.setDescription(description);
        ControllerBackEnd.modifierTache(tache);
        backEndResponse.setResponseCode(1);
        return backEndResponse;

    }

    static BackEndResponse getTache(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        long id = Long.parseLong(request.getParameter("id"));
        String[] keys = {"nom", "description"};
        Tache tache = ControllerBackEnd.getTache(id);
        String[] values = {tache.getNom(), tache.getDescription()};
        String tacheJSON = FrontEndUtility.jsonPropertyBuilder(keys, values);
        backEndResponse.setResponseCode(1);
        backEndResponse.setResponseBody(tacheJSON);
        return backEndResponse;

    }


    static BackEndResponse getTaches(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        ArrayList<Tache> listTache = ControllerBackEnd.getTaches();
        backEndResponse.setResponseCode(1);
        ArrayList<String> listJson = new ArrayList<>();
        String[] keys = {"id", "nom", "description"};

        for (Tache tache : listTache) {
            String[] values = {Long.toString(tache.getId()), tache.getNom(), tache.getDescription()};
            String tacheJSON = FrontEndUtility.jsonPropertyBuilder(keys, values);
            listJson.add(tacheJSON);
        }
        String response = FrontEndUtility.jsonPropertyBuilder("taches", listJson);
        backEndResponse.setResponseBody("{" + response + "}");

        return backEndResponse;

    }
}
