//Copyright (C) 2017  Nicolas Papanicolaou
//        This program is free software; you can redistribute it and/or modify
//        it under the terms of the GNU General Public License as published by
//        the Free Software Foundation; either version 3 of the License, or
//        (at your option) any later version.

//        This program is distributed in the hope that it will be useful,
//        but WITHOUT ANY WARRANTY; without even the implied warranty of
//        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//        GNU General Public License for more details.
//
//        You should have received a copy of the GNU General Public License
//        along with this program; if not, check https://www.gnu.org/licenses
package Controller.FrontEnd;

import Controller.BackEnd.ControllerBackEnd;
import Controller.BackEndResponse;
import Data.Employe;
import Data.Horaire;
import Data.Tache;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class ControllerHoraireFront {

    private static final String[] PLAGE_HORAIRE = {
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17"
    };
    private static final String JOUR = "2017-09-89";
    private static final String DEBUT = "8:00";
    private static final String FIN = "9:00";
    private static final String COMMENTAIRE = "No Commentaire";

    /**
     * The parameters needs to be in the HTTP requests body.
     *
     * @param request If the body is null, then parameters in body is treated as an empty string.
     * @return BackEndResponse
     * @see BackEndResponse
     */
    static BackEndResponse creationHoraire(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        int code = -1;
        try {
            HashMap map = FrontEndUtility.getHashMap(request);
            ArrayList<Long> arrayList = new ArrayList<>();
            for (String plageHoraire : PLAGE_HORAIRE){
                getTacheIdFromString(map,plageHoraire,arrayList);
            }
            long idEmploye = Long.parseLong(map.get("employeHoraire").toString());
            Horaire horaire = new Horaire(0, arrayList, idEmploye, JOUR, DEBUT, FIN, COMMENTAIRE);
            code = ControllerBackEnd.ajouterHoraire(horaire);
            backEndResponse.setResponseCode(code);
            backEndResponse.setResponseBody(FrontEndUtility.jsonPropertyBuilder("id", Long.toString(horaire.getId())));
            return backEndResponse;
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

        return backEndResponse;
    }

    static BackEndResponse modifierHoraire(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        HashMap map = FrontEndUtility.getHashMap(request);
        int code = -1;
        long idEmploye = Long.parseLong(map.get("employeHoraire").toString());
        Horaire horaire = ControllerBackEnd.getHoraire(idEmploye);
        if (horaire != null) {
            int i = 0;
            for (String plageHoraire : PLAGE_HORAIRE) {
                Object mapObject = map.get(plageHoraire);
                if (mapObject != null) {
                    if (!mapObject.toString().isEmpty()) {
                        Long idTache = Long.parseLong(mapObject.toString());
                        horaire.getTacheId().set(i, idTache);
                    }
                }
                i++;
            }
            code = ControllerBackEnd.modifierHoraire(horaire);
        }
        backEndResponse.setResponseCode(code);
        return backEndResponse;

    }

    private static void getTacheIdFromString(HashMap map, String plageHoraire, ArrayList arrayList) {
        String tacheString = (map.get(plageHoraire) == null) ? "" : map.get(plageHoraire).toString();
        long tacheId = tacheString.isEmpty() ? new Long(-1) : Long.parseLong(tacheString);
        arrayList.add(tacheId);
    }

    private static Date stringToDate(String dateString) {
        return new Date();
    }


    static BackEndResponse supprimerHoraire(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        String employeHoraireString = request.getParameter("id").toString();
        int code = -1;
        if (!employeHoraireString.isEmpty()) {
            code = 0;
            long id = Long.parseLong(employeHoraireString);
            ControllerBackEnd.supprimerHoraire(id);
            backEndResponse.setResponseCode(code);
        }
        backEndResponse.setResponseCode(code);
        return backEndResponse;

    }



    static BackEndResponse getHoraire(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        HashMap map = FrontEndUtility.getHashMap(request);
        long idEmploye = Long.parseLong(map.get("employeHoraire").toString());
        int code = -1;
        Horaire horaire = ControllerBackEnd.getHoraire(idEmploye);

        HashMap<Long, String> hashMapTache = new HashMap<>();
        ArrayList<Tache> tacheArrayList = ControllerBackEnd.getTaches();

        for (Tache tache : tacheArrayList) {
            hashMapTache.put(tache.getId(), tache.getNom());
        }

        Employe employe = ControllerBackEnd.getEmploye(idEmploye);
        if (employe != null){
            code = 0;
            ArrayList<Long> horaireTacheIdList = horaire.getTacheId();
            Object[] values = new Object[10];
            int countTache = 0;
            if (horaire != null) {
                for (Long idTache : horaireTacheIdList){
                    if (idTache != -1){
                        values[countTache] = hashMapTache.get(idTache);
                    } else {
                        values[countTache] = "";
                    }
                    countTache++;
                }
                String horaireJSON = FrontEndUtility.jsonPropertyBuilderArray(employe.getPrenom() + " " + employe.getNom(), values);
                backEndResponse.setResponseBody(horaireJSON);
            }
        }

        backEndResponse.setResponseCode(code);
        return backEndResponse;

    }


    static BackEndResponse getHoraires(HttpServletRequest request) {
        BackEndResponse backEndResponse = new BackEndResponse(0, null);
        ArrayList<Horaire> listHoraire = ControllerBackEnd.getHoraires();
        backEndResponse.setResponseCode(1);
        ArrayList<String> listJson = new ArrayList<>();
        HashMap<Long, String> hashMapTache = new HashMap<>();
        HashMap<Long, String> hashMapEmploye = new HashMap<>();
        ArrayList<Tache> tacheArrayList = ControllerBackEnd.getTaches();

        for (Tache tache : tacheArrayList) {
            hashMapTache.put(tache.getId(), tache.getNom());
        }

        ArrayList<Employe> employeArrayList = ControllerBackEnd.getEmployes();

        for (Employe employe : employeArrayList) {
            hashMapEmploye.put(employe.getId(), employe.getPrenom() + " " + employe.getNom());
        }

        for (Horaire horaire : listHoraire) {
            String nomEmploye = hashMapEmploye.get(horaire.getEmployeId());
            ArrayList<Long> arrayList = horaire.getTacheId();
            String tache1 = (arrayList.get(0) == -1) ? "" : hashMapTache.get(arrayList.get(0));
            String tache2 = (arrayList.get(1) == -1) ? "" : hashMapTache.get(arrayList.get(1));
            String tache3 = (arrayList.get(2) == -1) ? "" : hashMapTache.get(arrayList.get(2));
            String tache4 = (arrayList.get(3) == -1) ? "" : hashMapTache.get(arrayList.get(3));
            String tache5 = (arrayList.get(4) == -1) ? "" : hashMapTache.get(arrayList.get(4));
            String tache6 = (arrayList.get(5) == -1) ? "" : hashMapTache.get(arrayList.get(5));
            String tache7 = (arrayList.get(6) == -1) ? "" : hashMapTache.get(arrayList.get(6));
            String tache8 = (arrayList.get(7) == -1) ? "" : hashMapTache.get(arrayList.get(7));
            String tache9 = (arrayList.get(8) == -1) ? "" : hashMapTache.get(arrayList.get(8));
            String tache10 = (arrayList.get(9) == -1) ? "" : hashMapTache.get(arrayList.get(9));

            Object[] values = {tache1, tache2, tache3, tache4, tache5, tache6, tache7, tache8, tache9, tache10};
            String horaireJSON = FrontEndUtility.jsonPropertyBuilderArray(nomEmploye, values);
            listJson.add(horaireJSON);
        }
        String response = FrontEndUtility.jsonPropertyBuilder("data", listJson);
        backEndResponse.setResponseBody("{" + response + "}");

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{ \"data\": [");
        return backEndResponse;

    }
}
