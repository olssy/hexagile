# A qui le droit

## Ajouter employe

### Method : POST

### Path : `/api/ajouteEmploye`

### Return :

#### Status :
**SUCCES** : `200`

**ECHEC** : `417`

#### Json :
```
{ "id": [id-employe] }
```
## Μοdifier employe

### Method : POST

### Path : `/api/modifierEmploye`

### Return :

#### Status :
**SUCCES** : `200`

**ECHEC** : `404`

#### Json : NONE

## Supprimer employe\

### Method : DELETE

### Path : `/api/supprimerEmploye`
### Exemple path avec parametre : `/api/supprimerEmploye?numeroEmploye=1`

### Return :

#### Status :
**SUCCES** : `200`

**ECHEC** : `404`

#### Json : NONE

## Get liste d'employe

### Method : GET

### Path : `/api/getEmployes`

### Return :

#### Status :
**SUCCES** : `200`

**ECHEC** : `404`

#### Json :
```
{ "employes": [
{"id": "17","nom": "Son","prenom": "Goku","adresse": "South City","numeroTelephone": "1234123","courriel": "goku@aaa"},
{"id": "18","nom": "Son","prenom": "Gohan","adresse": "South City","numeroTelephone": "1234123","courriel": "gohan@aaa"}]}
```
## Get un employe
### Method : GET

### Path : `/api/getEmploye`
### Exemple path avec parametre : `/api/getEmploye?numeroEmploye=1`


### Return :

#### Status :
**SUCCES** : `200`

**ECHEC** : `404`

#### Json :

```
{"nom": "Son","prenom": "Chichi","adresse": "South City","numeroTelephone": "12312333","courriel": "chichichi@aaaa"}
```
