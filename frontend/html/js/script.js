// Precharge taches, horaires et employes
$.get( "/api/getTaches", function( response ) {
            taches = response;
});
$.get( "/api/getEmployes", function( response ) {
            employes = response;
});
$.get( "/api/getHoraires", function( response ) {
            horaires = response;
});

$( window ).resize(function() {
  $('table').width($(window).width());
});

$(document).ready(function() {

    while (typeof taches === 'undefined') {};// attendre les taches
    displayTaches();

    // enlever le submit by enter lorsqu<on a le focus dans un input
    $('input').keypress(function(e){
    if ( e.which == 13 ) { // pese ENTER
            $(this).next().focus();  // Focus ailleurs pour ne pas envoyer un submit non-ajax
            return false;
        }
    });

    // gif animer lors de chargement de ressoucre par des appels ajax
    $body = $("body");
    $( document ).ajaxStart(function() { 
        $body.addClass("loading");    
    });
    $( document ).ajaxStop(function() {
        $body.removeClass("loading");
    });


    /*********************************************************************
    /
    /   FONCTIONS
    /
    /********************************************************************/
    function getCellColor(cd) {
        var color = "#FFFFFF"; 
        $.each(taches, function(key, tachesArray) {
            $.each(tachesArray, function(key, tache) { 
                 if ( tache.nom === cd ) {
                    color = tache.description;
                    return;
                } 
            });
        });
        return color;
    }

    function getEmployes() {
        $.ajax({
            type: 'GET',
            url: '/api/getEmployes',
                success: function(response) {
                    employes = response;
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#message-erreur').empty().html(textStatus+" - "+jqXHR.status+": "+errorThrown+"  "+jqXHR.responseText);
                    dialogErreur.dialog('open');
                }
        });
    }

    function getEmploye(id) {
        var result;
        $.each(employes, function(key, employesArray) {
            $.each(employesArray, function(key, employe) { 
                if ( employe.id == id ) {
                    result = employe;
                    return false; // break loop
                }
            });
        });
        return result;
    };
    
    function getTache(id) {
        var result;
        $.each(taches, function(key, tachesArray) {
            $.each(tachesArray, function(key, tache) { 
                if ( tache.id == id ) {
                    result = tache;
                    return false; // break loop
                }
            });
        });
        return result;
    };

    function getHoraires(horaire) {
        // recuperer et valider champs
        $.ajax({
            type: 'GET',
            url: '/api/getHoraires',
                success: function(response) {
                    horaires = response;
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#message-erreur').empty().html(textStatus+" - "+jqXHR.status+": "+errorThrown+"  "+jqXHR.responseText);
                    dialogErreur.dialog('open');
                }
        });
    }

    function getHoraire(nom) {
        $.each( horaires.data, function( i, val ) {
            if (val[0] === nom) {
                horaire = val.slice(1, val.length);
            }
        });
    }

    function getTacheByNom(nom) {
        $.each(taches.taches, function( i, val ) {
            if( val.nom === nom ) {
                tache = val;
                return false; // sort du each()
            }
        });
    }

    function ajouterEmploye(employe) {
        // recuperer et valider champs
        $.ajax({
            type: 'POST',
            url: '/api/ajouterEmploye',
            data: employe.serialize(),
                success: function() {
                    getEmployes();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#message-erreur').empty().html(textStatus+" - "+jqXHR.status+": "+errorThrown+"  "+jqXHR.responseText);
                    dialogErreur.dialog('open');
                }
        });
    }
    
    function ajouterTache(tache) {
        // recuperer et valider champs
        $.ajax({
            type: 'POST',
            url: '/api/ajouterTache',
            data: tache.serialize(),
                success: function(data) {
                    var newTache = {};
                    newTache.nom = $('#formAjouterTache').find('input[name="nom"]').val();
                    newTache.description = $('#formAjouterTache').find('input[name="description"]').val();
                    newTache.id=data.id;
                    $('fieldset.taches > ul').append(createTacheHTML(newTache));
                    getTaches();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#message-erreur').empty().html(textStatus+" - "+jqXHR.status+": "+errorThrown+"  "+jqXHR.responseText);
                    dialogErreur.dialog('open');
                }
        });
    }

    function modifierEmploye() {
        // recuperer et valider champs
        $.ajax({
            type: 'POST',
            url: '/api/modifierEmploye',
            data: employe,
                success: function() {
                    table.ajax.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#message-erreur').empty().html(textStatus+" - "+jqXHR.status+": "+errorThrown+"  "+jqXHR.responseText);
                    dialogErreur.dialog('open');
                }
        });
    }

    function modifierTache() {
        // recuperer et valider champs
        $.ajax({
            type: 'POST',
            url: '/api/modifierTache',
            data: tache,
                success: function() {
                    getTaches();
                    table.ajax.reload();

                    // transforer cell de la table pour colorier les cell avec la tache
                    table.cells().every( function () {

                        var data = this.data();
                        if ( data === tache.nom) {

                            $(this.node()).css( 'background', tache.description );
                            $(this.node()).text(tache.nom);
                        }
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#message-erreur').empty().html(textStatus+" - "+jqXHR.status+": "+errorThrown+"  "+jqXHR.responseText);
                    dialogErreur.dialog('open');
                }
        });
    }

    function supprimerEmploye() {
        // recuperer et valider champs
        $.ajax({
            type: 'DELETE',
            url: '/api/supprimerEmploye?id='+employe.id,
                success: function() {
                    getEmployes();
                    getHoraires();
                    table.ajax.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#message-erreur').empty().html(textStatus+" - "+jqXHR.status+": "+errorThrown+"  "+jqXHR.responseText);
                    dialogErreur.dialog('open');
                }
        });
    }

    function supprimerTache() {
        safe = true;
        // transforer cell de la table pour colorier les cell avec la tache
        table.cells().every( function () {
        var data = this.data();
        if ( data === tache.nom) {
            $('#message-erreur').empty().html('<h1>Impossible de supprimer la tâche!</h1><p>La tâche est associée à une horaires, supprimer ou modifier les horaires contenant la tâche avant de supprimer la tâche.</p>');
            dialogErreur.dialog('open');
            safe = false;
        } else {
        // recuperer et valider champs
        }
      });
        if ( safe ) {
          $.ajax({
            type: 'DELETE',
            url: '/api/supprimerTache?id='+tache.id,
                success: function() {
                    getTaches();
                    table.cells().every( function () {
                        var data = this.data();
                        if ( data === tache.nom) {
                            $(this.node()).css( 'background', '#888888' );
                            $(this.node()).text("");
                        }
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#message-erreur').empty().html(textStatus+" - "+jqXHR.status+": "+errorThrown+"  "+jqXHR.responseText);
                    dialogErreur.dialog('open');
                }
           });
        }
    }

    function modifierHoraire(horaire) {
        $.ajax({
            type: 'POST',
            url: '/api/modifierHoraire',
            data: horaire.serialize(),
                success: function() {
                    getHoraires();
                    table.ajax.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#message-erreur').empty().html(textStatus+" - "+jqXHR.status+": "+errorThrown+"  "+jqXHR.responseText);
                    dialogErreur.dialog('open');
                }
        });

    }

    function supprimerHoraire() {
        // recuperer et valider champs
        $.ajax({
            type: 'DELETE',
            url: '/api/supprimerHoraire?id='+employe.id,
                success: function() {
                    getHoraires();
                    table.ajax.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#message-erreur').empty().html(textStatus+" - "+jqXHR.status+": "+errorThrown+"  "+jqXHR.responseText);
                    dialogErreur.dialog('open');
                }
        });
    }

    function createTacheHTML(tache) {
        return " <li class=\"tache-wrapper\" id=\"tacheListeId"+tache.id+"\">"+
                                                   "<div class=\"tache\">"+
                                                     "<div class=\"couleur\" style=\"background: "+tache.description+"\">"+
                                                      "</div>"+
                                                      "<div class=\"id\"></div>"+
                                                      "<div class=\"titreTache\">"+
                                                          tache.nom+
                                                      "</div>"+
                                                    "</div>"+
                                                  "</li>";

    }

    // chercher les taches du serveur
    function getTaches() {
        $.get( "/api/getTaches", function( response ) {
            taches = response;
            displayTaches();
        });
        
    }

    function displayTaches() {
        $('fieldset.taches > ul ').empty();

        $.each(taches.taches, function(key, tache) {
            $('fieldset.taches > ul').append(createTacheHTML(tache));
        });
    }
  
    function ajouterHoraire(horaire) {
        $.ajax({
            type: 'POST',
            url: '/api/ajouterHoraire',
            data: horaire.serialize(),
                success: function() {
                    getHoraires();
                    table.ajax.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#message-erreur').empty().html(textStatus+" - "+jqXHR.status+": "+errorThrown+"  "+jqXHR.responseText);
                    dialogErreur.dialog('open');
                }
        });
    }

    function validerEmploye(value) {
        // Code Postal
        var regex = /[A-Za-z]\d[A-Za-z] ?\d[A-Za-z]\d/;
        var match = regex.exec(value);
        if (match) {
            return true;
        }
        else {//return false;
            $('#cp_erreur').dialog({
                autoOpen:false,
                buttons: {
                    "OK": function() {
                        ajouterEmploye(employe);
                        $( this ).dialog( "close" );
                    }
                }
            });
            return false;
        }
    }

    function employeHasHoraire(employe) {
        var found = false;
        $.each(horaires.data, function ( idx, hor ) {
            if ( hor[0] === employe.prenom+' '+employe.nom ) {
                found = true;
                return false; // exit each()
            }
        });
        return found;
    }

    function validerHoraire(Horaire) {

    }

    function validerTache(tache) {

    }


     table = $('#horaires').DataTable(  {
        ajax: '/api/getHoraires',
        dom: '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"Blfr>'+
             't'+
             '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',
        paging:         false,
        ordering: false,
        "language": {
	    "sProcessing":     "Traitement en cours...",
	    "sSearch":         "Rechercher&nbsp;:",
            "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
	    "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
	    "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
	    "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
	    "sInfoPostFix":    "",
	    "sLoadingRecords": "Chargement en cours...",
            "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
	    "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
	    "oPaginate": {
		"sFirst":      "Premier",
		"sPrevious":   "Pr&eacute;c&eacute;dent",
		"sNext":       "Suivant",
		"sLast":       "Dernier"
	    },
	    "oAria": {
		"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
		"sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
	    }
        },
        "columnDefs": [{
            "targets": '_all',
            "createdCell": function (td, cellData, rowData, row, col) {
                var color = getCellColor(cellData); //(cellData === 'Pause') ? 'blue' : 'red';
                $(td).css('background', color);
            }
        }]
    } );

    // Boutons de la table dans le toolbar
    new $.fn.dataTable.Buttons( table, {
        buttons: [
            'copy', 'excel', 'pdf'
        ]
    } );

    // highlight les cell de la table sur mouseouver
    $('#horaires tbody').on( 'mouseenter', 'td', function () {
            //var colIdx = table.cell(this).index().column;
 
            //$( table.cells().nodes() ).removeClass( 'highlight' );
            //$( table.column( colIdx ).nodes() ).addClass( 'highlight' );
        } );


    // Inserer le datepicker dans le toolbar de la table
    $("div.calendrier").html('<div class="datepicker" id="datepicker">');

    // Calendrier
    $( "#datepicker" ).datepicker({
        onSelect: function(dateText) {
            //date =  $.datepicker.formatDate('yy/mm/dd', $(this).val());
            $("span.date").text(dateText);
        }
    });
    $("#datepicker").datepicker('setDate', new Date());
    $("#datepicker").datepicker( $.datepicker.regional[ "fr" ] ); 
    $('#dateAjouterHoraire').datepicker( $.datepicker.regional["fr"]);
    $('#dateSupprimerHoraire').datepicker( $.datepicker.regional["fr"]);
    $('#dateSupprimerHoraire').on("change",function(){
        selectedDate = $(this).val();
    });

    /**********************************************************
    /
    /     Dialogues
    /
    /*********************************************************/

    $('#dialogueAjouterEmploye').dialog({
        autoOpen:false,
        open: function(event, ui) {
            $('#blocker').addClass("block-ui");    
            $('fieldset input[type="text"]').val('');
        },
        close: function(e, ui) {
            $('#blocker').removeClass("block-ui");    

        },
        buttons: {
          "Soumettre": function() {

              employe= $('#formAjouterEmploye');

              if ( validerEmploye($('#formAjouterEmploye input[name=postal]').val()) ) {
                  ajouterEmploye(employe);
                  $( this ).dialog( "close" );
              }
              else {
                  $('#message-erreur').empty().text('Le code postal est invalide.');
                  dialogErreur.dialog('open');
              }
          },
          "Annuler": function() {

            $( this ).dialog( "close" );
          }
        }
    });
    
    // dialogue modifier employe
    $('#dialogueModifierEmploye').dialog({
        autoOpen:false,
        minWidth: 300,
        open: function( event, ui ) {
            $('#blocker').addClass("block-ui");    
            $('fieldset input[type="text"]').val('');
            $("select[name='modifierEmploye']").empty().append(
                                          '<option selected="selected" disabled="true">--Choisir--</option>');
            $.each(employes, function(key, employesArray) {
                $.each(employesArray, function(key, emp) { 
                    $("select[name='modifierEmploye']").append(" <option value='"+emp.id+"'>"+emp.prenom +
                                                               " "+emp.nom+" ("+
                                                               emp.id+")</option>");
                });
            });



            selectModEmp = $('#listeModifierEmployes').selectmenu({
maxHeight:300,
                select: function( event, ui ) {
                    var idEmploye = ui.item.value;
                    if (idEmploye !== '--Choisir--') { //parfoir --Choisir-- est automatiquement selectionne
                        vieuxEmploye = employe = getEmploye(idEmploye);
                        $('#prenom_modifierEmploye').val(employe.prenom);
                        $('#nomDeFamille_modifierEmploye').val(employe.nom);
                        $('#courriel_modifierEmploye').val(employe.courriel);
                        $('#adresse_modifierEmploye').val(employe.adresse);
                        $('#postal_modifierEmploye').val(employe.postal);
                        $('#telephone_modifierEmploye').val(employe.telephone);
                    }
                }
            });
            $(selectModEmp).val('--Choisir--');
            $(selectModEmp).selectmenu('refresh');
        },
        close: function(e, ui) {
            $('#blocker').removeClass("block-ui");    

        },
        buttons: {
            "Soumettre": function() {
                employe.prenom = $('#prenom_modifierEmploye').val();
                employe.nom = $('#nomDeFamille_modifierEmploye').val();
                employe.courriel = $('#courriel_modifierEmploye').val();
                employe.adresse = $('#adresse_modifierEmploye').val();
                employe.postal = $('#postal_modifierEmploye').val();
                employe.telephone = $('#telephone_modifierEmploye').val();

                if ( validerEmploye($('#postal_modifierEmploye').val()) ) {
                    modifierEmploye();
                    $( this ).dialog( "close" );
                }
                else {
                    $('#message-erreur').empty().text('Le code postal est invalide.');
                    dialogErreur.dialog('open');
                }
            },
            "Annuler": function() {
                $( this ).dialog( "close" );
            }
        }
    });

    // dialogue supprimer employe
    $('#dialogueSupprimerEmploye').dialog({
        autoOpen:false,
        minWidth: 300,
        open: function( event, ui ) {
            $('#blocker').addClass("block-ui");    
            $('fieldset input[type="text"]').val('');
            $("select[name='supprimerEmploye']").empty().append(
                                          '<option selected="selected" disabled="true">--Choisir--</option>');
            $.each(employes, function(key, employesArray) {
                $.each(employesArray, function(key, employe) { 
                    $("select[name='supprimerEmploye']").append(
       " <option value='"+employe.id+"'>"+employe.prenom +" "+employe.nom+" ("+employe.id+")</option>");
                    });
                });
                selectSupEmp = $("#listeSupprimerEmployes").selectmenu( {
maxHeight:300,
                    select: function( event, ui ) {
                        var idEmploye = ui.item.value;
                        employe = getEmploye(idEmploye);
                    }
                });
                $(selectSupEmp).val('--Choisir--');
                $(selectSupEmp).selectmenu('refresh');
        },
        close: function(e, ui) {
            $('#blocker').removeClass("block-ui");    

        },
        buttons: {
          "Soumettre": function() {
             supprimerEmploye();
             $( this ).dialog( "close" );
          },
          "Annuler": function() {
            $( this ).dialog( "close" );
          }
        }
    });

    // dialogue modifier tache
    $('#dialogueModifierTache').dialog({
        autoOpen:false,
        minWidth: 300,
        open: function( event, ui ) {
            $('#blocker').addClass("block-ui");    

            $('fieldset input[type="text"]').val('');

            $("select[name='modifierTache']").empty();
            $("select[name='modifierTache']").append(
                                            '<option selected="selected" disabled="true">--Choisir--</option>');
            $.each(taches, function(key, tachesArray) {
                $.each(tachesArray, function(key, tac) { 
                    $("select[name='modifierTache']").append(
                                                " <option value='"+tac.id+"'>"+tac.nom+"("+tac.id+")</option>");
                });
            });

            if ( typeof selectModTache === 'undefined' ) {
                selectModTache = $('#listeModifierTache').selectmenu({
maxHeight:300,
                    select: function( event, ui ) {
                        if ( ui.item.value !== "--Choisir--" ) {
                            var idTache = ui.item.value;
                            tache = getTache(idTache);

                            $('#nom_modifierTache').val(tache.nom);
                            $('#description_modifierTache').val(tache.description);
                        } else {
                            $('#nom_modifierTache').val('');
                            $('#description_modifierTache').val('');
                        }
                    }
                });
            } else {
                selectModTache.val("--Choisir--");
                selectModTache.selectmenu('refresh');
            }
        },
        close: function(e, ui) {
            $('#blocker').removeClass("block-ui");    

        },
        buttons: {
          "Soumettre": function() {
               tache.nom = $('#nom_modifierTache').val();
               tache.description = $('#description_modifierTache').val();
               modifierTache();
               $( this ).dialog( "close" );
          },
          "Annuler": function() {
               $( this ).dialog( "close" );
          }
        }
    });
    // dialogue supprimer tache
    $('#dialogueSupprimerTache').dialog({
        autoOpen:false,
        minWidth: 300,
        open: function( event, ui ) {
                $('#blocker').addClass("block-ui");    

                $('fieldset input[type="text"]').val('');

                $("select[name='supprimerTache']").empty().append(
                                          '<option selected="selected" disabled="true">--Choisir--</option>');

                $.each(taches, function(key, tachesArray) {
                    $.each(tachesArray, function(key, tache) { 
                        $("select[name='supprimerTache']").append(" <option value='"+tache.id+"'>"+tache.nom +
                                                                    "("+
                                                                    tache.id+")</option>");
                    });
                });
                if ( typeof selectSupTache === 'undefined' ) {
                  selectSupTache = $("#listeSupprimerTache").selectmenu( {
maxHeight:300,
                    select: function( event, ui ) {
                        var idTache = ui.item.value;
                        tache = getTache(idTache);
                    }
                  });
                } else {
                  selectSupTache.val("--Choisir--");
                  selectSupTache.selectmenu('refresh');
                }
        },
        close: function(e, ui) {
            $('#blocker').removeClass("block-ui");    
        },
        buttons: {
          "Soumettre": function() {
             supprimerTache();

             $( this ).dialog( "close" );
          },
          "Annuler": function() {
            $( this ).dialog( "close" );
          }
        }
    });
    // dialogue ajouter tache 
    $('#dialogueAjouterTache').dialog({
        autoOpen:false,
        minWidth: 300,
        open: function(event, ui) {
            $('#blocker').addClass("block-ui");    
            $('fieldset input[type="text"]').val('');
        },
        close: function(e, ui) {
            $('#blocker').removeClass("block-ui");    

        },
        buttons: {
            "Soumettre": function() {
                tache = $('#formAjouterTache');
                validerTache(tache);
                ajouterTache(tache);
                $( this ).dialog( "close" );
            },
            "Annuler": function() {
                $( this ).dialog( "close" );
            }
        }
    });

    // dialogue supprimer horaire
    $('#dialogueSupprimerHoraire').dialog({
        autoOpen:false,
        minWidth: 300,
        open: function( event, ui ) {
            $('#blocker').addClass("block-ui");
            $('fieldset input[type="text"]').val('');
            $("select[name='supprimerHoraire']").empty().append(
                                          '<option selected="selected" disabled="true">--Choisir--</option>');
            $.each(employes, function(key, employesArray) {
                $.each(employesArray, function(key, employe) { 
                    if ( employeHasHoraire(employe) ) {
                        $("select[name='supprimerHoraire']").append(" <option value='"+employe.id+"'>"+employe.prenom +
                                                                " "+employe.nom+" ("+
                                                                    employe.id+")</option>");
                    }
                });
            });
            selectSupHor = $("#listeSupprimerHoraire").selectmenu( {
maxHeight:300,
                select: function( event, ui ) {
                    var idEmploye = ui.item.value;
                    employe = getEmploye(idEmploye);
                }
            });
            $(selectSupHor).val('--Choisir--');
            $(selectSupHor).selectmenu('refresh');
        },
        close: function(e, ui) {
            $('#blocker').removeClass("block-ui");    
        },
        buttons: {
            "Soumettre": function() {
                 if ( typeof employe !== 'undefined' ) {
                     supprimerHoraire();
                     $( this ).dialog( "close" );
                 }
            },
            "Annuler": function() {
                $( this ).dialog( "close" );
            }
        }
    });

    // dialogue ajouter horaire
    $('#dialogueAjouterHoraire').dialog({
        autoOpen:false,
        minWidth: 300,
        open: function( event, ui ) {
            $('#blocker').addClass("block-ui");    

            $('fieldset input[type="text"]').val('');

            $("select[name='employeHoraire']").empty().append(
                                     '<option selected="selected" disabled="true" value="0">--Choisir--</option>');

            $.each(employes, function(key, employesArray) {
                $.each(employesArray, function(key, employe) { 
                    if ( !employeHasHoraire(employe) ) {
                        $("select[name='employeHoraire']").append(" <option value='"+employe.id+"'>"+employe.prenom+
                                                                  " " +employe.nom+"</option>");
                    }
                });
            });

            if ( typeof selectAjouterEmpHor === 'undefined' ) {
                selectAjouterEmpHor = $("#listeEmployeHoraire").selectmenu( {
maxHeight:300,
                    select: function( event, ui ) {
                        var idEmploye = ui.item.value;
                        employe = getEmploye(idEmploye);
                    }
                });
            } else {
                selectAjouterEmpHor.selectmenu('refresh');
            }

            $("select.selectAjouterHoraire").each(function(){$(this).empty().append(
                                           '<option vlaue="" selected="selected" disabled="true">--Choisir--</option>'+
                                           '<option value="-1">Non assignée</option>')});;
            $.each(taches, function(key, tachesArray) {
                $.each(tachesArray, function(key, tache) { 
                    $("select.selectAjouterHoraire").append(" <option value='"+tache.id+"'>"+tache.nom+"</option>");
                });
            });

	    if ( typeof arraySelectAjouterHoraire === 'undefined' ) {
                arraySelectAjouterHoraire = $('select.selectAjouterHoraire').selectmenu({
maxHeight:300
});
	    } else {
                arraySelectAjouterHoraire.each(function(i) { 
	            $(this).selectmenu('refresh');
                });
	    }
        },
        close: function(e, ui) {
            $('#blocker').removeClass("block-ui");    
        },
        buttons: {
            "Soumettre": function() {
                horaire = $('#formAjouterHoraire');
                validerHoraire(horaire);
                ajouterHoraire(horaire);
                $( this ).dialog( "close" );
            },
            "Annuler": function() {
                $( this ).dialog( "close" );
            }
        }
    });

    // dialogue modifier horaire
    $('#dialogueModifierHoraire').dialog({
        autoOpen:false,
        minWidth: 300,
        open: function( event, ui ) {

            $('#blocker').addClass("block-ui");    

            $('fieldset input[type="text"]').val('');

            $("select[name='employeHoraire']").empty().append(
                                    '<option selected="selected" disabled="true" value="0">--Choisir--</option>');

            $.each(employes, function(key, employesArray) {
                $.each(employesArray, function(key, employe) { 
                    if ( employeHasHoraire(employe) ) {
                        $("select[name='employeHoraire']").append(" <option value='"+employe.id+"'>"+employe.prenom+
                                                                  " " +employe.nom+"</option>");
                    }
                });
            });

	    if ( typeof selectModifierEmpHor === 'undefined' ) {
                selectModifierEmpHor = $("#listeEmployeModifHoraire").selectmenu( {
maxHeight:300,
                    select: function( event, ui ) {
                        if ( selectModifierEmpHor.val() !== null ) {
                            var idEmploye = ui.item.value;
                            employe = getEmploye(idEmploye);
                            getHoraire(employe.prenom+' '+employe.nom);
                        }
                        // boucle a travewrs les plages horaires
                        $.each( horaire, function(ind, nomTache) {
                            if ( nomTache !== "" && nomTache !=='--Choisir--') { // si on a une tache
                                getTacheByNom(nomTache);  // cheche la tache au complet
                                $(arraySelectModifierHoraire[ind]).val(tache.id);  // mettre a jour e select
                                $(arraySelectModifierHoraire[ind]).selectmenu('refresh'); // rafraichir le select
                            }
                        });
                    }
                });
	    } else {
	        selectModifierEmpHor.selectmenu('refresh');
	    }

            $("select.selectModHoraire").each(function(){$(this).empty().append(
                                           '<option vlaue="" selected="selected" disabled="true">--Choisir--</option>'+
                                           '<option value="-1">Non assignée</option>')
            });
            $.each(taches, function(key, tachesArray) {
                $.each(tachesArray, function(key, tache) { 
                    $("select.selectModHoraire").append(" <option value='"+tache.id+"'>"+tache.nom+"</option>");
                });
            });

	    if ( typeof arraySelectModifierHoraire === 'undefined' ) {
                arraySelectModifierHoraire = $('select.selectModHoraire').selectmenu({
maxHeight:300
});
	    } else {
                arraySelectModifierHoraire.each(function(i) { 
                    $(this).val("--Choisir--");
                    $(this).selectmenu('refresh');
                });
            }
        },
        close: function(e, ui) {
            $('#blocker').removeClass("block-ui");    
        },
        buttons: {
            "Soumettre": function() {
                 horaire = $('#formModifierHoraire');
                 //validerHoraire(horaire);
                 modifierHoraire(horaire);
                 $( this ).dialog( "close" );
             },
             "Annuler": function() {
                 $( this ).dialog( "close" );
             }
        }
    });

    // dialog erreur
    dialogErreur = $('#dialog-erreur').dialog({
        autoOpen: false,
        minWidth: 300,
        title: "Une erreur est survenue",
        open: function(event, ui) {
            $('#blocker').addClass("ui-widget-shadow ui-widget-overlay");    
            
        },
        close: function(event, ui) {
            $('#blocker').removeClass("ui-widget-shadow ui-widget-overlay");    
            
        },
        buttons: {
            "Ok": function() {
                 $( this ).dialog( "close" );
            }
        }
    });

    // Click listeners pour Employes
    $( "#boutonAjouterEmploye" ).click(function(e) {
        $('#dialogueAjouterEmploye').dialog('open');
    });

    $( "#boutonModifierEmploye" ).click(function() {
        $('#dialogueModifierEmploye').dialog('open');
    });

    $( "#boutonSupprimerEmploye" ).click(function() {
        $('#dialogueSupprimerEmploye').dialog('open');
    });


    // Click listeners pour Taches 
    $( "#boutonAjouterTache" ).click(function() {
        $('#dialogueAjouterTache').dialog('open');
    });
    $( "#boutonModifierTache" ).click(function() {
        $('#dialogueModifierTache').dialog('open');
    });
    $( "#boutonSupprimerTache" ).click(function() {
        $('#dialogueSupprimerTache').dialog('open');
    });

    // Click listeners pour Horaire 
    $( "#boutonAjouterHoraire" ).click(function() {
        $('#dialogueAjouterHoraire').dialog('open');
    });
    $( "#boutonModifierHoraire" ).click(function() {
        $('#dialogueModifierHoraire').dialog('open');
    });
    $( "#boutonSupprimerHoraire" ).click(function() {
        $('#dialogueSupprimerHoraire').dialog('open');
    });
    $( "#boutonQuitter" ).click(function() {
        window.location = 'logout.jsp';
    });
  $('table').width($(window).width()-7);
} );
